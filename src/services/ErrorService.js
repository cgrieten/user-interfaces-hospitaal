import { Notify } from 'quasar'
const ErrorService = {
  showAlert (userMessage) {
    Notify.create({
      color: 'negative',
      position: 'top',
      message: userMessage,
      icon: 'report_problem'
    })
  },
  showAlertConnection (userMessage) {
    Notify.create({
      color: 'negative',
      position: 'top',
      message: userMessage,
      icon: 'report_problem',
      timeout: 1000
    })
  },
  notify (userMessage) {
    Notify.create({
      position: 'top',
      message: userMessage,
      icon: 'announcement'
    })
  },
  log (error) {
    console.log(error)
  }
}

export default ErrorService
