import firebase from '../../firebase'
import ErrorService from '../../services/ErrorService'

const state = {
  patients: []
}

const getters = {
  getPatients: (state) => state.patients,
  getPatient: (state) => (id) => state.patients.find(p => p.id === id + ''),
  getPatientByRoomnumber: (state) => (number) => state.patients.find(p => p.roomnumber === number + ''),
  getPatientAction: (state) => (number) => {
    let patient = state.patients.find(p => p.roomnumber === number + '')
    let actions = patient.actions
    let action = Object
    let nearest = null
    let i = Number
    if (actions !== undefined) {
      for (let index = 0; index < actions.length; index++) {
        action = actions[index]
        let time = new Date(action.timestamp)
        if (!action.done) {
          if (nearest === null) {
            nearest = action
            i = index
          } else if (time < new Date(nearest.timestamp)) {
            nearest = action
            i = index
          }
        }
      }
      return actions[i]
    }
    return undefined
  },
  checkPatient: (state) => (id) => {
    const patient = state.patients.find(p => p.id === id + '')
    return patient !== undefined
  }
}

const actions = {
  async fetchPatients ({ commit }) {
    firebase.database.ref('persons').on('value', snapshot => {
      const patients = []
      const obj = snapshot.val()
      for (const key in obj) {
        patients.push({
          id: key,
          name: obj[key].name,
          heartbeat: obj[key].heartbeat,
          bloodpressure: obj[key].bloodpressure,
          actions: obj[key].actions,
          comments: obj[key].comments,
          department: obj[key].department,
          vegetarian: obj[key].vegetarian,
          roomnumber: obj[key].roomnumber,
          hospitalisationdate: obj[key].hospitalisationdate,
          hospitalisationreason: obj[key].hospitalisationreason,
          photopath: obj[key].photopath,
          doctor: obj[key].doctor
        })
      }
      commit('setPatients', patients)
    }, error => {
      ErrorService.showAlertConnection('Er is iets misgegaan bij het ophalen van de patienten', error)
    })
  },
  async updatePatient ({ commit }, updatedPatient) {
    firebase.database.ref('persons').child(updatedPatient.id).update(updatedPatient).catch(error => ErrorService.showAlert('Deze patient bestaat niet' + error))
  }
}

const mutations = {
  setPatients: (state, patients) => (state.patients = patients)
}

export default {
  state,
  getters,
  actions,
  mutations
}
