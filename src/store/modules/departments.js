import firebase from '../../firebase'
import ErrorService from '../../services/ErrorService'

const state = {
  departments: [],
  rooms: [],
  loadingDepartments: false
}

const getters = {
  getAllDepartments: (state) => state.departments,
  getDepartmentByName: (state) => (departmentName) => state.departments.find(d => d.name === departmentName),
  checkDepartment: (state) => (departmentName) => {
    const depart = state.departments.find(d => d.name === departmentName)
    return depart !== undefined
  },
  getLoadingDepartment: (state) => state.loadingDepartments
}

const actions = {
  fetchDepartments ({ commit }) {
    commit('setLoadingDepartments', true)
    firebase.database.ref('departments').on('value', snapshot => {
      commit('setDepartments', snapshot.val())
      commit('setLoadingDepartments', false)
    }, error => {
      ErrorService.showAlertConnection('De afdelingen kunnen niet geladen worden', error)
      commit('setLoadingDepartments', true)
    })
  }
}

const mutations = {
  setDepartments: (state, departments) => (state.departments = departments),
  setRooms: (state, rooms) => (state.rooms = rooms),
  setLoadingDepartments: (state, bool) => (state.loadingDepartments = bool)
}

export default {
  state,
  getters,
  actions,
  mutations
}
