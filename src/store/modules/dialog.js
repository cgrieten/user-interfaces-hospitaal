const state = {
  prompt: false,
  newAction: { type: '', description: '', timestamp: '' },
  checkNew: false,
  type: '',
  timestamp: '',
  description: '',
  id: -1
}

const getters = {
  getPrompt: (state) => {
    return state.prompt
  },
  getAction: (state) => state.newAction,
  getCheckNew: (state) => state.checkNew,
  getActionType: (state) => state.type,
  getActionDescription: (state) => state.description,
  getActionTimestamp: (state) => state.timestamp,
  getActionId: (state) => state.id
}

const actions = {

}

const mutations = {
  setPrompt (state, bool) {
    state.prompt = bool
  },
  setAction (state, a) {
    state.newAction = a
  },
  setCheckNew (state, cn) {
    state.checkNew = cn
  },
  setActionType (state, cn) {
    state.type = cn
  },
  setActionDescription (state, cn) {
    state.description = cn
  },
  setActionTimestamp (state, cn) {
    state.timestamp = cn
  },
  setActionId (state, cn) {
    state.id = cn
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
