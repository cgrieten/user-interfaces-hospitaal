const state = {
  alarmCheck: true,
  bloodPressure: { min: 88, max: 128 },
  heartRate: { min: 70, max: 130 },
  actionMinutes: 120,
  roomSettings: { number: true, facilities: true, patient: true, monitoring: true, action: true },
  colors: { vrij: '#FFFFFF', bezet: '#008000', hulp: '#8c0f0f', actionStart: '#ff0000', actionEnd: '#c9ff00' }
}

const getters = {
  getAlarmCheck: (state) => state.alarmCheck,
  getMinBloodPressure: (state) => state.bloodPressure.min,
  getMaxBloodPressure: (state) => state.bloodPressure.max,
  getMinHeartRate: (state) => state.heartRate.min,
  getMaxHeartRate: (state) => state.heartRate.max,
  getActionMinutes: (state) => state.actionMinutes,
  getRoomSettings: (state) => state.roomSettings,
  getColors: (state) => state.colors
}

const actions = {

}

const mutations = {
  setAlarmCheck: (state) => (state.alarmCheck = !state.alarmCheck),
  setMinBloodPressure (state, val) {
    state.bloodPressure.min = val
  },
  setMaxBloodPressure (state, val) {
    state.bloodPressure.max = val
  },
  setMinHeartRate (state, val) {
    state.heartRate.min = val
  },
  setMaxHeartRate (state, val) {
    state.heartRate.max = val
  },
  setActionMinutes (state, val) {
    state.actionMinutes = val
  },
  setRoomSettings (state, val) {
    state.roomSettings = val
  },
  setColors (state, val) {
    state.colors = val
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
