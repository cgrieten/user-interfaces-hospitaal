import Vue from 'vue'
import Vuex from 'vuex'
import dialog from './modules/dialog'
import firebase from '../firebase'
import patients from './modules/patients'
import settings from './modules/settings'
import departments from './modules/departments'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)
Vue.use(firebase)

export default new Vuex.Store({
  modules: {
    departments: departments,
    patients: patients,
    settings: settings,
    dialog: dialog
  },
  plugins: [createPersistedState()]
})
