import { initializeApp, database } from 'firebase'

const firebaseConfig = {
  apiKey: 'AIzaSyAfAUFn8JJ7YI4h6m7orArHFc7T6GkG92A',
  authDomain: 'hospitaal-5f94b.firebaseapp.com',
  databaseURL: 'https://hospitaal-5f94b.firebaseio.com',
  projectId: 'hospitaal-5f94b',
  storageBucket: 'hospitaal-5f94b.appspot.com',
  messagingSenderId: '552906989993',
  appId: '1:552906989993:web:063d8888c5a9102d1924b8'
}
initializeApp(firebaseConfig)

export default {
  database: database()
}
