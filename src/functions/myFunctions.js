import { date } from 'quasar'

export function getFormattedDate (d, includeTime) {
  if (includeTime) {
    return date.formatDate(d, 'HH:mm DD/MM/YYYY')
  } else {
    return date.formatDate(d, 'DD/MM/YYYY')
  }
}

export function getDate (d) {
  return date.formatDate(d, 'YYYY-MM-DD HH:mm')
}
