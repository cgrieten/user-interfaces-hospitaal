
const routes = [
  {
    path: '/',
    redirect: to => '/departments'
  },
  {
    path: '/departments',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/departments/:department', component: () => import('pages/Department.vue') }
    ]
  },
  {
    path: '/patients/:patientid',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Patient.vue') }
    ]
  },
  {
    path: '/settings',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '/settings', component: () => import('pages/Settings.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '*', component: () => import('pages/Error404.vue') }
    ]
  })
}

export default routes
